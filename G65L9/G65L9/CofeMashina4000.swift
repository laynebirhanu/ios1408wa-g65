//
//  CofeMashina4000.swift
//  G65L9
//
//  Created by Ivan Vasilevich on 9/11/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit

class CofeMashina4000: NSObject {
	
	static let shared = CofeMashina4000() //designated initializor
	
	var coffe: String!
	var melk: String!
	var elements = ["coffe", "melk"]
	
	var getElements: [String] {
		var result = [String]()
		result += [coffe, melk]
		return result
	}
	

}
