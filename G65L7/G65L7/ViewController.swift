//
//  ViewController.swift
//  G65L7
//
//  Created by Ivan Vasilevich on 9/4/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

	@IBOutlet weak var messageTextField: UITextField!
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//		if segue.identifier == ""
		if let alligatorVC = segue.destination as? AlligatorVC,
			let stringFromSender = sender as? String {
			alligatorVC.message = stringFromSender
		}
	}
	@IBAction func barButtonPressed(_ sender: Any) {
		showAlligatorVC()
	}
	
	@IBAction func buttonPressed() {
		showAlligatorVC()
	}
	
	func showAlligatorVC() {
		performSegue(withIdentifier: "showAlligator", sender: "momomo")
	}
}

