//
//  GreenVC.swift
//  G61L7
//
//  Created by Ivan Vasilevich on 2/26/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit

class GreenVC: UIViewController {
	
	
	@IBOutlet weak var centerButton: UIButton!
	var stringFromPrevVC = ""
	
    override func viewDidLoad() {
        super.viewDidLoad()

		log()
		print(stringFromPrevVC)
		print(navigationItem)
        // Do any additional setup after loading the view.
		centerButton.setTitle(stringFromPrevVC, for: .normal)
    }
	
	@IBAction func goBack() {
		dismiss(animated: true) { // for modal presentation
			print("green vc gone")
		}
		navigationController?.popViewController(animated: true)
	}
	
//	override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
//		return identifier == "yes"
//	}
	
	

}
